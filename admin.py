from django.contrib import admin

from .models import Baby, Feeding

from utilities.admin import NameAndSlugAdmin


class BabyAdmin(NameAndSlugAdmin):
    pass


class FeedingAdmin(admin.ModelAdmin):
    list_display = ["baby", "when", "amount"]
    list_filter = ["when"]

admin.site.register(Baby, BabyAdmin)
admin.site.register(Feeding, FeedingAdmin)
