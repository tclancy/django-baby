from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext

from .forms import FeedingForm
from .models import Baby


def add_feeding(request, slug):
    baby = get_object_or_404(Baby, slug=slug)
    if request.POST:
        form = FeedingForm(request.POST)
        if form.is_valid():
            feeding = form.save(commit=False)
            feeding.baby = baby
            feeding.save()
            return redirect('add_feeding', slug=slug)
    else:
        form = FeedingForm()
    feedings = baby.feedings.all()[:10]
    return render_to_response('baby/feeding.html',
                              {'form': form, 'feedings': feedings, 'baby': baby},
                              context_instance=RequestContext(request))
