from django.conf.urls.defaults import patterns, url

urlpatterns = patterns(
    'baby.views',
    url(r'^(?P<slug>[^/]+)/$', 'add_feeding', name='add_feeding'),
)
