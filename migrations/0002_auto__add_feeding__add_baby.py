# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Feeding'
        db.create_table('baby_feeding', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('baby', self.gf('django.db.models.fields.related.ForeignKey')(related_name='feedings', to=orm['baby.Baby'])),
            ('when', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('amount', self.gf('django.db.models.fields.FloatField')()),
        ))
        db.send_create_signal('baby', ['Feeding'])

        # Adding model 'Baby'
        db.create_table('baby_baby', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=200)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=500)),
        ))
        db.send_create_signal('baby', ['Baby'])


    def backwards(self, orm):
        # Deleting model 'Feeding'
        db.delete_table('baby_feeding')

        # Deleting model 'Baby'
        db.delete_table('baby_baby')


    models = {
        'baby.baby': {
            'Meta': {'ordering': "['name']", 'object_name': 'Baby'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '500'})
        },
        'baby.feeding': {
            'Meta': {'object_name': 'Feeding'},
            'amount': ('django.db.models.fields.FloatField', [], {}),
            'baby': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'feedings'", 'to': "orm['baby.Baby']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'when': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['baby']