from django import forms

from .models import Feeding


class FeedingForm(forms.ModelForm):
    def clean_amount(self):
        amount = self.cleaned_data.get('amount', -10.0)
        if amount > 8.0 or amount < 0.1:
            raise forms.ValidationError('Please check that amount')
        return amount

    class Meta:
        model = Feeding
        fields = ['amount', ]
