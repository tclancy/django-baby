from django.db import models

from utilities.models import NameAndSlug


class Baby(NameAndSlug):
    class Meta:
        verbose_name_plural = 'Babies'


class Feeding(models.Model):
    baby = models.ForeignKey(Baby, related_name='feedings')
    when = models.DateTimeField(auto_now_add=True)
    amount = models.FloatField(help_text='in ounces')

    class Meta:
        ordering = ['-when']
